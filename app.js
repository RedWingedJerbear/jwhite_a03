const path = require("path")
const express = require("express")
const logger = require("morgan")
const bodyParser = require("body-parser")
const fs = require('fs')
const app = express()  // make express app
const port = 8081
const config = require('./config.json')

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) //Path to views
app.set("view engine", "ejs") //specify view

// 2 include public assets and use bodyParser
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// 3 set up the logger
// log HTTP requests to a file using the standard Apache combined format
// see https://github.com/expressjs/morgan for more
let accessLogStream = fs.createWriteStream(__dirname + '/access.log', { flags: 'a' });
app.use(logger('dev'));
app.use(logger('combined', { stream: accessLogStream }));

// 4 handle valid GET requests
//default page
app.get("/", function (req, res) {
  res.render("index.ejs")
})

//tic tac toe page
app.get("/tic-tac-toe", function (req, res) {
  res.render("tic-tac-toe.ejs")
})

//contact page
app.get("/contact", function (req, res) {
  res.render("contact-me.ejs")
})

// 5 handle valid POST request
app.post("/contact", function (req, res) {
  var api_key = config.MAILGUN_API_KEY || process.env.API_KEY;
  var domain = config.MAILGUN_DOMAIN || process.env.DOMAIN;
  var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });

  var data = {
    from: 'Mail Gun Myapp <postmaster@sandboxe83faf1fecb44754a4ccb4ff2a4ca096.mailgun.org>',
    to: 'white@nwmissouri.edu',
    subject: req.body.name,
    text: req.body.message
  };

  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if (!error)
      res.send("Mail Sent");
    else {
      res.send("Mail not sent");
      console.log(error);
    }
  });
})

// 6 respond with 404 if a bad URI is requested
app.get(function (req, res) {
  res.render("404")
})

// Listen for an application request on port 8081
app.listen(process.env.PORT || port, function () {
  console.log('app listening on http://127.0.0.1:' + port)
})
